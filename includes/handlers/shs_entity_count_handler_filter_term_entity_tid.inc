<?php

/**
 * @file
 * Definition of shs_entity_count_handler_filter_term_entity_tid.
 */

/**
 * Filter by term id (including selection by simple hierarchical select).
 *
 * @ingroup views_filter_handlers
 */
class shs_entity_count_handler_filter_term_entity_tid extends shs_handler_filter_term_node_tid {

  function option_definition() {
    $options = parent::option_definition();

    // Add "entity_count" setting to shs widgets.
    $options['expose']['contains'] += array(
      'shs' => array(
        'entity_count' => array('default' => FALSE),
        'entity_count_settings' => array(
          'entity_count_children' => array('default' => FALSE),
          'entity_count_entity_types' => array(),
        ),
      ),
    );
    $entity_info = entity_get_info();
    $entity_options = array();
    foreach ($entity_info as $entity => $entity_info) {
      $entity_options[$entity] = array('entity_type_count_' . $entity => array('default' => FALSE));
    }
    $options['expose']['contains']['shs']['entity_count_settings']['entity_count_entity_types'] += $entity_options;

    return $options;
  }

  function expose_form(&$form, &$form_state) {
    parent::expose_form($form, $form_state);
    // Add the entity count settings form.
    _shs_entity_count_settings_form($form['expose']['shs'], $this->options['expose']['shs']);
  }

}
