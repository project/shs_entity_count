<?php

/**
 * @file
 * Entity count functionality for Simple hierarchical select.
 */

/**
 * Implements hook_field_widget_info_alter().
 */
function shs_entity_count_field_widget_info_alter(&$info) {
  if (empty($info['taxonomy_shs'])) {
    return;
  }
  // Add "entity_count" setting to shs widgets.
  $info['taxonomy_shs']['settings']['shs'] += array(
    'entity_count' => FALSE,
    'entity_count_settings' => array(
      'entity_count_children' => FALSE,
      'entity_count_entity_types' => array(),
    ),
  );
  $entity_info = entity_get_info();
  $options = array();
  foreach ($entity_info as $entity => $entity_info) {
    $options[$entity] = array('entity_type_count_' . $entity => FALSE);
  }
  $info['taxonomy_shs']['settings']['shs']['entity_count_settings']['entity_count_entity_types'] += $options;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function shs_entity_count_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if (empty($form['instance']['widget']['settings']['shs'])) {
    return;
  }
  $settings_form = &$form['instance']['widget']['settings']['shs'];
  $settings = &$form['#instance']['widget']['settings']['shs'];

  _shs_entity_count_settings_form($settings_form, $settings);

}

/**
 * Implements hook_shs_term_get_children_alter().
 */
function shs_entity_count_shs_term_get_children_alter(&$terms, &$alter_options) {
  if (empty($alter_options['settings']['entity_count']) || empty($alter_options['settings']['language']) || !isset($alter_options['parent'])) {
    // Nothing to do here.
    return;
  }

  $langcode = $alter_options['settings']['language']->language;
  $parent = $alter_options['parent'];

  $entity_types = $alter_options['settings']['entity_count_settings']['entity_count_entity_types'];
  $selected_bundles = array();
  foreach ($entity_types as $entity_type => $bundles) {
    $selected_bundles[$entity_type] = array_filter(array_shift($bundles));
  }
  $selected_bundles = array_filter($selected_bundles);
  $hash = md5(serialize($selected_bundles));

  $term = (object) array(
    'vid' => $alter_options['vid'],
    'tid' => 0,
  );
  $format = variable_get('shs_entity_count_format', '%s (%d)');
  $count_children = $alter_options['settings']['entity_count_settings']['entity_count_children'];

  foreach ($terms as &$item) {
    array_walk($item[$langcode][$parent], function (&$name, $key) use ($term, $format, $selected_bundles, $count_children, $hash) {
      $term->tid = $key;
      $cache_key = 'tid:' . $key . ':bundles:' . $hash;
      // Get cached values.
      $cache = cache_get($cache_key, 'cache_shs_entity_count');
      if (!$cache || ($cache->expire && time() > $cache->expire)) {
        $count = _shs_entity_count_term_get_entity_count($term, $selected_bundles, $count_children);
        // Set cached data.
        cache_set($cache_key, $count, 'cache_shs_entity_count', CACHE_PERMANENT);
      }
      else {
        // Use cached data.
        $count = $cache->data;
      }
      $name = sprintf($format, $name, $count);
    });
  }
}

/**
 * Implements hook_field_delete_instance().
 */
function shs_entity_count_field_delete_instance($instance) {

  if ($hash = _shs_entity_count_get_selected_bundles_hash_from_field_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
    $query = db_select('{taxonomy_entity_index}', 'taxonomy_entity_index');
    $query->fields('taxonomy_entity_index', array('tid'));
    $query->condition('taxonomy_entity_index.field_name', $instance['field_name']);
    $query->condition('taxonomy_entity_index.entity_type', $instance['entity_type']);
    $query->condition('taxonomy_entity_index.bundle', $instance['bundle']);
    $results = $query->execute();

    foreach ($results as $result) {
      $terms_to_refresh = taxonomy_get_parents_all($result->tid);
      foreach ($terms_to_refresh as $term_to_refresh) {
        cache_clear_all('tid:' . $term_to_refresh->tid . ':bundles:' . $hash, 'cache_shs_entity_count');
      }
    }
  }
}

/**
 * Implements hook_taxonomy_term_delete().
 */
function shs_entity_count_taxonomy_term_delete($term) {
  // Remove all cache cids of this term.
  $terms_to_refresh = taxonomy_get_parents_all($term->tid);
  foreach ($terms_to_refresh as $term_to_refresh) {
    cache_clear_all('tid:' . $term_to_refresh->tid . ':', 'cache_shs_entity_count', TRUE);
  }
}

/**
 * Implements hook_entity_delete().
 */
function shs_entity_count_entity_delete($entity, $entity_type) {
  _shs_entity_count_clear_cache_for_terms_on_entity($entity_type, $entity);
}

/**
 * Implements hook_field_attach_insert().
 */
function shs_entity_count_field_attach_insert($entity_type, $entity) {
  _shs_entity_count_clear_cache_for_terms_on_entity($entity_type, $entity);
}

/**
 * Implements hook_field_attach_update().
 */
function shs_entity_count_field_attach_update($entity_type, $entity) {
  _shs_entity_count_clear_cache_for_terms_on_entity($entity_type, $entity);
}

/**
 * Implements hook_views_data_alter().
 */
function shs_entity_count_views_data_alter(&$data) {
  // Add filter handler for term ID with depth.
  foreach (entity_get_info() as $type => $info) {
    if ($info['fieldable'] && isset($data[$info['base table']])) {
      $data[$type]['shs_entity_count_term_entity_tid_depth'] = array(
        'help' => t('Display content if it has the selected taxonomy terms, or children of the selected terms. Due to additional complexity, this has fewer options than the versions without depth. Optionally the filter will use a simple hierarchical select for the selection of terms.'),
        'real field' => $info['entity keys']['id'],
        'filter' => array(
          'title' => t('Has taxonomy terms (with depth; @type): Entity count', array('@type' => 'Simple hierarchical select')),
          'handler' => 'shs_entity_count_handler_filter_term_entity_tid_depth',
        ),
      );
    }
  }
}

/**
 * Implements hook_field_views_data_alter().
 */
function shs_entity_count_field_views_data_alter(&$result, &$field, $module) {
  if (empty($field['columns']) || !in_array($field['type'], array('taxonomy_term_reference', 'entityreference'))) {
    return;
  }
  if ($field['type'] == 'entityreference' && (empty($field['settings']['target_type']) || $field['settings']['target_type'] != 'taxonomy_term')) {
    // Do not change entityreference fields that do not reference terms.
    return;
  }
  $field_column = key($field['columns']);
  foreach ($result as $key => $group) {
    $field_identifier = sprintf('%s_%s', $field['field_name'], $field_column);
    if (empty($group[$field_identifier]) || empty($group[$field_identifier]['filter']['handler'])) {
      // Only modify field definitions for the primary column.
      continue;
    }
    // Replace handler.
    $result[$key][$field_identifier]['filter']['handler'] = ($field['type'] == 'entityreference') ? 'shs_entity_count_handler_filter_entityreference' : 'shs_entity_count_handler_filter_term_entity_tid';
  }
}

/**
 * Helper function to count number of entities associated to a term.
 *
 * @param object $term
 *   The term object.
 * @param object $selected_bundles
 *   The bundles per entity type that need to be counted.
 * @param boolean $count_children
 *   If set to TRUE, entities in child terms are counted also.
 *
 * @return int
 *   Number of entities within the term.
 */
function _shs_entity_count_term_get_entity_count($term, $selected_bundles, $count_children = FALSE) {
  $num_entities = 0;
  
  if (!empty($selected_bundles)) {
    $index_table = 'taxonomy_entity_index';

    // Count entities associated to this term.
    $query = db_select($index_table, 'ti');
    $query->fields('ti');
    $query->condition('ti.tid', $term->tid);
    $db_or = db_or();
    foreach ($selected_bundles as $entity_type => $bundles) {
      $db_and = db_and();
      $db_and->condition('ti.entity_type', $entity_type);
      $db_and->condition('ti.bundle', $bundles , 'IN');
      $db_or->condition($db_and);
    }
    $query->condition($db_or);
    $result = $query->execute();

    $num_entities = $result->rowCount();

    if ($count_children) {
      $tids = array();
      $tree = taxonomy_get_tree($term->vid, $term->tid);
      foreach ($tree as $child_term) {
        $tids[] = $child_term->tid;
      }
      if (count($tids)) {
        // Count entities associated to child terms.
        $query = db_select($index_table, 'ti');
        $query->fields('ti');
        $query->condition('ti.tid', $tids, 'IN');
        $db_or = db_or();
        foreach ($selected_bundles as $entity_type => $bundles) {
          $db_and = db_and();
          $db_and->condition('ti.entity_type', $entity_type);
          $db_and->condition('ti.bundle', $bundles , 'IN');
          $db_or->condition($db_and);
        }
        $query->condition($db_or);
        $result = $query->execute();

        $num_entities += $result->rowCount();
      }
    }
  }

  return $num_entities;
}

/**
 * Function to clear cache for terms on entity.
 *
 * @param string $entity_type
 *   The entity type.
 * @param object $entity
 *   The entity for which to clear term count cache.
 */
function _shs_entity_count_clear_cache_for_terms_on_entity($entity_type, $entity) {
  list($entity_id, $revision_id, $bundle) = entity_extract_ids($entity_type, $entity);

  $taxonomy_fields = taxonomy_entity_index_get_taxonomy_field_names($entity_type, $bundle);
  if (empty($taxonomy_fields)) {
    return;
  }

  foreach ($taxonomy_fields as $field_name) {
    $items = field_get_items($entity_type, $entity, $field_name);
    $original_items = isset($entity->original) ? field_get_items($entity_type, $entity->original, $field_name) : FALSE;
    if ($items && $items != $original_items) {
      if ($hash = _shs_entity_count_get_selected_bundles_hash_from_field_instance($entity_type, $field_name, $bundle)) {
        foreach ($items as $delta => $item) {
          $terms_to_refresh = taxonomy_get_parents_all($item['tid']);
          foreach ($terms_to_refresh as $delta => $term_to_refresh) {
            cache_clear_all('tid:' . $term_to_refresh->tid . ':bundles:' . $hash, 'cache_shs_entity_count');
          }
        }
        if ($original_items) {
          foreach ($original_items as $delta => $original_item) {
            $terms_to_refresh = taxonomy_get_parents_all($original_item['tid']);
            foreach ($terms_to_refresh as $delta => $term_to_refresh) {
              cache_clear_all('tid:' . $term_to_refresh->tid . ':bundles:' . $hash, 'cache_shs_entity_count');
            }
          }
        }
      }
    }
  }
}

/**
 * Helper function to construct the selected bundles hash.
 *
 * @param string $entity_type
 *   The entity type on which the taxonomy term was used.
 * @param string $field_name
 *   The field in which the taxonomy term was used.
 * @param string $bundle
 *   The bundle of the entity on which the taxonomy term was used.
 *
 * @return string $hash
 *   Returns a hashed string or FALSE.
 */
function _shs_entity_count_get_selected_bundles_hash_from_field_instance($entity_type, $field_name, $bundle) {
  $field_info_instance = field_info_instance($entity_type, $field_name, $bundle);
  if (isset($field_info_instance['widget']['settings']['shs']['entity_count_settings']['entity_count_entity_types'])) {
    $entity_types = $field_info_instance['widget']['settings']['shs']['entity_count_settings']['entity_count_entity_types'];
    $selected_bundles = array();
    foreach ($entity_types as $entity_type => $bundles) {
      $selected_bundles[$entity_type] = array_filter(array_shift($bundles));
    }
    $selected_bundles = array_filter($selected_bundles);
    $hash = md5(serialize($selected_bundles));
    return $hash;
  }

  return FALSE;
}

/**
 * Helper function to construct the settings form.
 *
 * @param array $settings_form
 *   The settings form for which we are adding extra settings.
 * @param array $settings
 *   An array of setting for our form.
 */
function _shs_entity_count_settings_form(&$settings_form, $settings) {
  $entity_info = entity_get_info();

  $settings_form['entity_count'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display number of entities'),
    '#description' => t('Display the number of entities associated with the term. Do not forget to check which entities should be counted.'),
    '#default_value' => empty($settings['entity_count']) ? FALSE : $settings['entity_count'],
    '#weight' => -1,
  );
  $settings_form['entity_count_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity count settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      // Set form visibility in widget settings or in exposed filter settings.
      'visible' => array(
        array(
          array(':input[name="instance[widget][settings][shs][entity_count]"]' => array('checked' => TRUE)),
          'or',
          array(':input[name="options[expose][shs][entity_count]"]' => array('checked' => TRUE)),
        ),
      ),
    '#weight' => -1,
    )
  );
  $settings_form['entity_count_settings']['entity_count_children'] = array(
    '#type' => 'checkbox',
    '#title' => t('Also count children of term.'),
    '#description' => t('If checked this will result in a larger number because the children will be counted also.'),
    '#default_value' => empty($settings['entity_count_settings']['entity_count_children']) ? FALSE : $settings['entity_count_settings']['entity_count_children'],
  );
  $settings_form['entity_count_settings']['entity_count_entity_types'] = array(
    '#type' => 'item',
    '#title' => t('Select entities that should be counted.'),
    '#description' => t('Select entity type or one of it\'s bundles that should be counted'),
  );

  foreach ($entity_info as $entity => $entity_info) {
    if (!empty($entity_info['bundles']) && $entity_info['fieldable'] === TRUE) {
      $options = array();
      $settings_form['entity_count_settings']['entity_count_entity_types'][$entity] = array(
        '#type' => 'fieldset',
        '#title' => check_plain($entity_info['label']),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
        $options[$bundle] = $bundle_info['label'];
      }
      $settings_form['entity_count_settings']['entity_count_entity_types'][$entity]['entity_type_count_' . $entity] = array(
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => empty($settings['entity_count_settings']['entity_count_entity_types'][$entity]['entity_type_count_' . $entity]) ? FALSE : $settings['entity_count_settings']['entity_count_entity_types'][$entity]['entity_type_count_' . $entity],
      );
    }
  }
}
